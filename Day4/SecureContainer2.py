#
# 112233 meets these criteria because the digits never decrease and all repeated digits are exactly two digits long.
# 123444 no longer meets the criteria (the repeated 44 is part of a larger group of 444).
# 111122 meets the criteria (even though 1 is repeated more than twice, it still contains a double 22).
#


range_start = 367479
range_end = 893698
j = 0

possible_passwords = 0

for i in range(range_start, range_end):
    digit_list = list(map(int, str(i)))

    total_valid = False
    double_valid = False

    for j in range(0, 5):
        valid = digit_list[j] <= digit_list[j + 1]

        if not valid:
            total_valid = False
            break
        else:
            total_valid = True

    if total_valid:
        for j in range(0, 5):
            valid = digit_list[j] == digit_list[j + 1]

            if not valid:
                continue
            else:
                double_valid = True

        if double_valid:
            possible_passwords += 1

print(possible_passwords)

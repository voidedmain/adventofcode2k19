# Author:   marinaperkunic
# Version:  1.0
# Python:   3.x

range_start = 367479
range_end = 893698
j = 0

possible_passwords = 0

for i in range(range_start, range_end):

    # the 6 digit long integer is not iterable,
    # so each integer needs its digits to be stored
    # in a list in order o be iterable
    digit_list = list(map(int, str(i)))

    # for integers who's digits are incremental from left to right
    total_valid = False

    # for integers who have at least one digit that repeats itself directly after itself
    double_valid = False

    # searching for all total_valid integers
    for j in range(0, 5):
        valid = digit_list[j] <= digit_list[j + 1]

        if not valid:
            total_valid = False
            break
        else:
            total_valid = True

    # searching for all double_valid integer in the total_valid
    if total_valid:
        for j in range(0, 5):
            valid = digit_list[j] == digit_list[j + 1]

            if not valid:
                continue
            else:
                double_valid = True

        if double_valid:
            possible_passwords += 1

print(possible_passwords)

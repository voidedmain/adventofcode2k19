# Author:   marinaperkunic
# Version:  1.0
# Python:   3.x

with open('input', 'r') as input_file:
    mass_of_modules = input_file.readlines()

fuel_requirement = 0


def additional_fuel_for_module(fuel):
    additional_required_fuel = 0

    # As long as the fuel is not 0 or negative 0, the fuel needs to be calculated for every additional fuel
    while fuel > 0:

        fuel = int(int(fuel) / 3) - 2

        # if the fuel hits below 0, it is changed to 0 fuel
        if fuel < 0:
            fuel = 0

        additional_required_fuel += fuel

    return additional_required_fuel


for mass in mass_of_modules:
    required_fuel_for_module = int(int(mass) / 3) - 2

    # Total fuel for all modules
    fuel_requirement += required_fuel_for_module

    # Calculates the fuel for the additional fuel and adds it to the total fuel for all modules
    fuel_requirement += additional_fuel_for_module(required_fuel_for_module)

print(fuel_requirement)

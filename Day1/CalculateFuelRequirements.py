# Author:   marinaperkunic
# Version:  1.0
# Python:   3.x

with open('input', 'r') as input_file:
    mass_of_modules = input_file.readlines()

fuel_requirement = 0

for mass in mass_of_modules:
    required_fuel_for_module = int(int(mass) / 3) - 2

    fuel_requirement += required_fuel_for_module

print(fuel_requirement)
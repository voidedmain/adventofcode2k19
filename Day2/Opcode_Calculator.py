# Author:   marinaperkunic
# Version:  1.0
# Python:   3.x

# Reading a file with open() as turns it into a list of Strings. To access every entry as an Integer,
# each String needs to be mapped as an Integer and then stored into a list again.
with open('input', 'r') as opcodes_input:
    opcodes_input = opcodes_input.read().split(',')
    opcodes_input = list(map(int, opcodes_input))

i = 0

# Opcode 99 program is finished and should immediately halt
while opcodes_input[i] is not 99:
    # Opcode 1 adds together numbers
    if opcodes_input[i] == 1:
        # integer1, integer2  =   the first two integers after the Opcode indicate the positions
        #                         from which you should read the input values
        integer1 = opcodes_input[opcodes_input[i + 1]]
        integer2 = opcodes_input[opcodes_input[i + 2]]

        sum_of = integer1 + integer2

        # result_place  =   and the third indicates the position at which the output should be stored
        result_place = opcodes_input[i + 3]
        opcodes_input[result_place] = sum_of
    # Opcode 2 multiplies the two inputs instead of adding them
    else:
        if opcodes_input[i] == 2:
            # integer1, integer2  =   the first two integers after the Opcode indicate the positions
            #                         from which you should read the input values
            integer1 = opcodes_input[opcodes_input[i + 1]]
            integer2 = opcodes_input[opcodes_input[i + 2]]

            product_of = integer1 * integer2

            # result_place  =   and the third indicates the position at which the output should be stored
            result_place = opcodes_input[i + 3]
            opcodes_input[result_place] = product_of
        # "catches" the program if Opcode is something else than 1 or 2
        else:
            if opcodes_input[i] == 99:
                break
            else:
                print('Something went wrong...')
                break

    # Once you're done processing an opcode, move to the next one by stepping forward 4 positions
    i = i + 4

print(opcodes_input)
# Author:   marinaperkunic
# Version:  1.0
# Python:   3.x

# Reading a file with open() as turns it into a list of Strings. To access every entry as an Integer,
# each String needs to be mapped as an Integer and then stored into a list again.
with open('input', 'r') as opcodes_input:
    opcodes_input = opcodes_input.read().split(',')
    opcodes_input = list(map(int, opcodes_input))

i = 0
verb = 0
noun = 0

for noun in range(0, 99):
    for verb in range(0, 99):
        opcodes_input[1] = noun
        opcodes_input[2] = verb

        while opcodes_input[i] is not 99:
            if opcodes_input[i] == 1:
                integer1 = opcodes_input[opcodes_input[i + 1]]
                integer2 = opcodes_input[opcodes_input[i + 2]]

                sum_of = integer1 + integer2

                result_place = opcodes_input[i + 3]
                opcodes_input[result_place] = sum_of

                if opcodes_input[0] == 19690720:
                    print('Special Product: ' + str(opcodes_input[0]))

                    print('integer1: ' + str(integer1))
                    print('integer2: ' + str(integer2))

                    print('Sum: ' + str(100 * noun + verb))

                    break
            else:
                if opcodes_input[i] == 2:
                    integer1 = opcodes_input[opcodes_input[i + 1]]
                    integer2 = opcodes_input[opcodes_input[i + 2]]

                    product_of = integer1 * integer2

                    result_place = opcodes_input[i + 3]
                    opcodes_input[result_place] = product_of

                    # only if the value at position 0 is exactly 19690720 the program calculates the sum
                    if opcodes_input[0] == 19690720:
                        print('Special Product: ' + str(opcodes_input[0]))

                        print('integer1: ' + str(integer1))
                        print('integer2: ' + str(integer2))

                        print('Sum: ' + str(100 * noun + verb))

                        break
                else:
                    if opcodes_input[i] == 99:
                        print('Special Product: ' + str(opcodes_input[0]))
                        break
                    else:
                        print('Something went wrong...')

            i = i + 4

        i = 0

        # resets the list after each try by reading the original file again
        with open('input', 'r') as opcodes_input:
            opcodes_input = opcodes_input.read().split(',')
            opcodes_input = list(map(int, opcodes_input))
